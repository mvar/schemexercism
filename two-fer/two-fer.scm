(define-module (two-fer)
  #:export (two-fer))

(define* (two-fer #:optional (name "you"))
  (list->string
   (append
    (append (string->list "One for ") (string->list name))
    (string->list ", one for me.")
    )
   )
  )
