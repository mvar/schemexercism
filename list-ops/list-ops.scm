(define-module (list-ops)
  #:export (my-length
            my-reverse
            my-map
            my-filter
            my-fold
            my-append
            my-concatenate
            ))

(define (my-length s)
  (if (null? s)
      (+ 0)
      (+ 1 (my-length (cdr s)))
      )
  )

(define (my-reverse ls)
  (my-reverse-rec ls '())
  )

(define (my-reverse-rec ls0 ls1)
  (if (null? ls0)
      ls1
      (my-reverse-rec (cdr ls0) (cons (car ls0) ls1)
                      )
      )
  )

(define (my-map f l)
  (if (null? l)
      '()
      (cons (f (car l)) (my-map f (cdr l)))
      )
  )

(define (my-filter f s)
  (if (null? s)
      '()
      (if (f (car s))
          (cons (car s) (my-filter f (cdr s)))
          (my-filter f (cdr s))
          )
      )
  )

(define (my-fold op acc l)
  (if (null? l)
      acc
      (op (car l) (my-fold op acc (cdr l)))
      )
  )

(define (my-append a b)
  (cond ((null? a) b)
        ((null? b) a)
        (else
         (if (null? (cdr a))
            (cons (car a) b)
            (cons (car a) (my-append (cdr a) b))
            )
        ))
  )

(define (my-concatenate ll)
  (my-fold my-append '() ll)
  )
