(define-module (hamming)
  #:export (hamming-distance))

(define (hamming-distance a b)
  (if (not (= (string-length a) (string-length b)))
      (throw (make-error))
      (count-ham a b 0)
      )
  )

(define (count-ham a b c)
  (if
   (or (= 0 (string-length a)) (= 0 (string-length b)))
   c
   (if (equal? (car (string->list a)) (car (string->list b)))
       (count-ham (list->string (cdr (string->list a)))
                  (list->string (cdr (string->list b)))
                  c
                  )
       (count-ham (list->string (cdr (string->list a)))
                  (list->string (cdr (string->list b)))
                  (+ 1 c)
                  )
       )
    )
  )

(begin
  (hamming-distance "test" "this")
  (newline))
