(define-module (squares)
  #:export (sum-of-squares
            square-of-sum
            difference)
  #:autoload (srfi srfi-1) (reduce iota))

(define (sum-of-squares n)
  (if (= n 0)
      0
      (+ (* n n) (sum-of-squares (- n 1)))
      )
  )

(define (square-of-sum n)
  (let ((a (reduce + 0 (iota (+ n 1)))))
    (* a a))
  )


(define (difference n)
  (- (square-of-sum n) (sum-of-squares n))
  )
